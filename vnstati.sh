#!/bin/sh
# Visualisierter Netzwerkverkehr mit Vnstat
# Ausgabe der Dateien in das Webserververzeichnis stats/traffic
# Scriptvorlage von http://www.gambaru.de/blog/2012/06/02/vnstat-und-vnstati-volumen-des-netzwerkverkehrs-ubersichtlich-visualisieren/
set -e

Target="$HOME/vnstati/"
DEVICE="mesh-vpn br-ffharz eth0 tun0" 

#HOME=/home/map
SSHHOSTUSER=user@hostname:/uploadpath/
SSHKEY=$HOME/.ssh/key
DATAP=$HOME/vnstati

for x in $DEVICE
do
	# stündlich
	/usr/bin/vnstati -h -i $x -o ${Target}${x}-vnstat_hourly.png
	# täglich
	/usr/bin/vnstati -d -i $x -o ${Target}${x}-vnstat_daily.png
	# monatlich
	/usr/bin/vnstati -m -i $x -o ${Target}${x}-vnstat_monthly.png
	# Top10
	/usr/bin/vnstati -t -i $x -o ${Target}${x}-vnstat_top10.png
	# Zusammenfassung
	/usr/bin/vnstati -s -i $x -o ${Target}${x}-vnstat_summary.png
done

scp -i $SSHKEY -r $DATAP/* $SSHHOSTUSER > /dev/null

